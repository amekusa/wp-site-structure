<?php
namespace amekusa\WPSiteStructure\node;
use amekusa\WPELib as wpe;

class TaxonomyNode extends Node {
	
	public function __construct($xTaxonomy, $xLabel = null) {
		parent::__construct(wpe\taxonomy($xTaxonomy), $xLabel);
	}
	
	public function getLabel() {
		$r = parent::getLabel();
		return $r ? $r : $this->entity->label;
	}
	
	public function getUrl() {
		return wpe\url_of_taxonomy($this->entity);
	}
}
?>