<?php
namespace amekusa\WPSiteStructure\node;
use amekusa\WPELib as wpe;

class PostTypeNode extends Node {
	
	public function __construct($xPostType, $xLabel = null) {
		parent::__construct($xPostType, $xLabel);
	}
	
	public function equals($xObject) {
		$r = parent::equals($xObject);
		if ($r) return true;
		if (!isset($xObject)) return false;
		
// 		if (isset($xObject->post_type)) {
// 			return $this->entity->name == $xObject->post_type;
// 		}
		
		return false;
	}
	
	public function isCurrent() {
		return is_post_type_archive($this->entity->name);
	}
	
	public function getTypeName() {
		return $this->entity->name;
	}
	
	public function getLabel() {
		$r = parent::getLabel();
		if ($r) return $r;
		
		//if ($this->isCurrent()) return $this->entity->post_title;
		//if ($this->isCurrent()) return get_queried_object()->post_title;
		return $this->entity->label;
	}
	
	public function getUrl() {
		return get_post_type_archive_link($this->entity->name);
	}
}
?>