<?php
namespace amekusa\WPSiteStructure\node;

use amekusa\PHPKnives as knv;
use amekusa\WPELib as wpe;

/**
 * A node consisting the site structure.
 *
 * <p lang="ja">仮想的なサイト構造を構成するノードを表します。</p>
 *
 * TODO equals() を isCurrent() に統合
 * TODO setParent() を protected に
 *
 * @author amekusa <post@amekusa.com>
 */
class Node {
	public $entity;
	public $children;
	public $parent;
	public $label;
	public $url = '';
	public $isInvisible;
	
	protected $level; // Hierarchical level
	
	public function __construct($xEntity = null, $xLabel = null, $xIsInvisible = false) {
		$this->entity = $xEntity;
		$this->label = $xLabel;
		$this->isInvisible = $xIsInvisible;
		$this->children = array ();
	}
	
	public function __get($xName) {
		return knv\get($xName, $this->entity);
	}
	
	public function __call($xName, $xArgs) {
		return call_user_func_array(array ($this->entity, $xName), $xArgs);
	}
	
	public function isCurrent() {
		$current = get_queried_object();
		return $this->equals($current);
	}
	
	public function isChild() {
		if (!$this->hasParent()) return false;
		
		$current = get_queried_object();
		return $this->parent->equals($current);
	}
	
	public function isSibling() {
		if (!$this->hasParent()) return false;
		
		$current = get_queried_object();
		foreach ($this->parent->children as $iSibling) {
			if ($iSibling->equals($current)) return true;
		}
		
		return false;
	}
	
	public function isParent() {
		if (!$this->hasChildren()) return false;
		
		$current = get_queried_object();
		foreach ($this->children as $iChild) {
			if ($iChild->equals($current)) return true;
		}
		
		return false;
	}
	
	public function isAscendant($xOf = null) {
		if (!$this->hasChildren()) return false;
		
		$of = isset($xOf) ? $xOf : $this->getCurrentNode();
		foreach ($this->children as $iChild) {
			if ($iChild->equals($of)) return true;
			if ($iChild->isAscendant($of)) return true;
		}
		
		return false;
	}
	
	public function hasChildren() {
		return !empty($this->children);
	}
	
	public function hasSiblings() {
		if (!$this->hasParent()) return false;
		return count($this->parent->children) > 1;
	}
	
	public function hasParent() {
		return isset($this->parent);
	}
	
	public function equals($xObject) {
		if ($xObject instanceof static) return $this->entity == $xObject->entity;
		
		else return $this->entity == $xObject;
	}
	
	public function findCurrent() {
		if (!$this->hasChildren()) return null;
		
		foreach ($this->children as $iChild) {
			if ($iChild->isCurrent()) return $iChild;
			$r = $iChild->findCurrent();
			if (isset($r)) return $r;
		}
		
		return null; // Not Found
	}
	
	public function findChild($xQuery) {
		if (!$this->hasChildren()) return null;
		
		foreach ($this->children as $iChild) {
			if ($iChild->equals($xQuery)) return $iChild;
			$r = $iChild->findChild($xQuery);
			if (isset($r)) return $r;
		}
		
		return null; // Not Found
	}
	
	public function getEntity() {
		return $this->entity;
	}
	
	protected function getSubject() {
		return get_queried_object();
	}
	
	public function getPosition($xFrom = null) {
		$main = get_queried_object();
		
		if ($this->isCurrent()) return 'current';
		if ($this->isChild()) return 'child';
		if ($this->isParent()) return 'parent';
		if ($this->isSibling()) return 'sibling';
		
		return ''; // No Relation
	}
	
	public function getLabel() {
		return isset($this->label) ? $this->label : '';
	}
	
	public function getUrl() {
		return $this->url;
	}
	
	public function getChildren() {
		return $this->children;
	}
	
	public function getChild($xIndex) {
		return $this->children[$xIndex];
	}
	
	public function getCurrentNode() {
		$main = get_queried_object();
		
		foreach ($this->children as $iCh) {
			if ($iCh->equals($main)) return $iCh;
			
			$iR = $iCh->getCurrentNode();
			if (isset($iR)) return $iR;
		}
		
		return null;
	}
	
	public function getParent() {
		return $this->parent;
	}
	
	public function getAscendants($xExcludesInvisibles = false) {
		$r = array ();
		$parent = $this->parent;
		
		while (isset($parent)) {
			if (!$xExcludesInvisibles || !$parent->isInvisible) array_unshift($r, $parent);
			
			$parent = $parent->parent;
		}
		
		return $r;
	}
	
	public function addChild($xNode, $xIndex = null) {
		if ($xNode->hasParent()) throw new \Exception('ParentDuplication');
		if (!isset($xIndex)) $this->children[] = $xNode;
		else $this->children[$xIndex] = $xNode;
		$xNode->setParent($this);
	}
	
	public function setParent($xNode) {
		$this->parent = $xNode;
	}
	
	public function setAscendant($xNode) {
		
	}
	
	public function setDescendant($xNode) {
		
	}
}
?>