<?php
namespace amekusa\WPSiteStructure\node;
use amekusa\WPELib as wpe;

class TermStructureNode extends TaxonomyNode {
	
	public function __construct($xTaxonomy) {
		parent::__construct($xTaxonomy);
	}
	
	public function buildStructure() {
		$term = $this->entity;
		$parentTerm = wpe\parent_of_term($term);
		
		while ($parentTerm) {
			$parent = new TermNode($parentTerm);
			
			$parentTerm = wpe\parent_of_term($parentTerm);
		}
	}
}
?>