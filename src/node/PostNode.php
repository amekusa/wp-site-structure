<?php
namespace amekusa\WPSiteStructure\node;
use amekusa\WPELib as wpe;

class PostNode extends Node {
	
	public function __construct($xPost, $xLabel = null, $xIsInvisible = false) {
		parent::__construct($xPost, $xLabel, $xIsInvisible);
	}
	
	public function isParent() {
		if (parent::isParent()) return true;
		$subject = $this->getSubject();
		return $this->equals(wpe\parent_of_post($subject));
	}
	
	public function equals($xObject) {
		$post = wpe\post($xObject);
		if (is_null($post)) return false;
		return $this->entity->ID == $post->ID;
	}
	
	public function getLabel() {
		$r = parent::getLabel();
		return $r ? $r : apply_filters('the_title', $this->entity->post_title, $this->entity->ID);
	}
	
	public function getUrl() {
		return wpe\url_of_post($this->entity);
	}
}
?>