<?php
namespace amekusa\WPSiteStructure\node;
use amekusa\WPELib as wpe;

class NotFoundNode extends Node {
	
	public function __construct($xLabel = null) {
		parent::__construct(null, $xLabel);
	}
	
	public function isCurrent() {
		return is_404();
	}
}
?>