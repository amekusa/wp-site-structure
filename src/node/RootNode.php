<?php
namespace amekusa\WPSiteStructure\node;
use amekusa\WPELib as wpe;

class RootNode extends Node {
	private static $instance;
	
	public static function getInstance() {
		if (!isset(static::$instance)) return new static();
		return static::$instance;
	}
	
	public function __construct($xLabel = null) {
		if (isset(static::$instance)) throw new \Exception('Multiple instanciation is not allowed');
		static::$instance = $this;
		
		$entity = null;
		
		if (get_option('show_on_front') == 'page') {
			$entity = wpe\post(get_option('page_on_front'));
		}
		
		parent::__construct($entity, $xLabel);
	}
	
	public function isCurrent() {
		return is_home() || is_front_page();
	}
	
	public function getLabel() {
		$r = parent::getLabel();
		return $r ? $r : $this->entity->post_title;
	}
	
	public function getUrl() {
		return home_url();
	}
}
?>