<?php
namespace amekusa\WPSiteStructure\node;
use amekusa\WPELib as wpe;

class TermNode extends Node {
	protected $termId;
	protected $taxName;
	
	public function __construct($xTerm, $xLabel = null) {
		parent::__construct($xTerm, $xLabel);
		$this->termId = $xTerm->term_id;
		$this->taxName = $xTerm->taxonomy;
	}
	
	public function getLabel() {
		$r = parent::getLabel();
		return $r ? $r : $this->getEntity()->name;
	}
	
	public function getUrl() {
		return wpe\url_of_term($this->getEntity());
	}
	
	public function getEntity() {
		if ($this->entity) return $this->entity;
		return wpe\term($this->termId, $this->taxName);
	}
}
?>