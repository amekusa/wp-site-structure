<?php
namespace amekusa\WPSiteStructure;

use amekusa\PHPKnives as knv;
use amekusa\WPELib as wpe;

interface functions {
	const required = true;
}

function root_node() {
	return node\RootNode::getInstance();
}

function current_node() {
	static $r = null;
	if ($r) return $r;
	
	$main = get_queried_object();
	if (!$main) return $r;
	
	$root = root_node();
	$r = $root->findChild($main);
	if ($r) return $r;
	
	if (is_singular()) {
		$r = new node\PostNode($main);
		
		if (is_post_type_hierarchical($main->post_type)) {
			$node = $r;
			$parent = wpe\parent_of_post($main);
			
			while ($parent) {
				$parentNode = new node\PostNode($parent);
				$parentNode->addChild($node);
				$node = $parentNode;
				$parent = wpe\parent_of_post($parent);
			}
			
			$root->addChild($node);
			
		} else {
			$parent = new node\PostTypeNode(wpe\type_of_post($main));
			$parent->addChild($r);
			$root->addChild($parent);
		}
	}
	
	return $r;
}
?>