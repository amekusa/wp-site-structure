<?php
namespace amekusa\WPSiteStructure\drafts;
use amekusa\WPSiteStructure as wps;
use amekusa\WPELib as wpe;

function render_breadcrumbs() {
	$r = '';
	
	$root = wps\node\RootNode::getInstance();
	$current = $root->getCurrentNode();
	
	if (!isset($current)) { // Not Found
		$qo = get_queried_object();
		
		if (is_page()) {
			$current = new wps\node\PostNode($qo);
			$parentPost = wpe\parent_of_post($qo);
			
			if (isset($parentPost)) {
				$parent = new wps\node\PostNode($parentPost);
				$parent->addChild($current);
				$root->addChild($parent);
				
			} else $root->addChild($current);
			
		} if (is_404()) {
			$current = new wps\node\Node(null, '404 Not Found');
			$root->addChild($current);
			
		} else $current = $root;
	}
	
	$ascendants = $current->getAscendants();
	ob_start()?>
<ol class="wp-site-structure-breadcrumbs">
	<?php foreach ($ascendants as $i => $iEach) {?>
	<li>
		<?php $iLabel = $iEach->getLabel()?>
		<?php $iUrl = $iEach->getUrl()?>
		<a href="<?php echo esc_url($iUrl)?>"><?php echo $iLabel?></a>
	</li>
	<?php }?>
	<li><?php echo $current->getLabel()?></li>
</ol>
	<?php $r = ob_get_clean();
	
	echo $r;
}

function nav_menu_css_class($xClasses, $xItem, $xArgs) {
	//var_dump($xItem);
	//$x = $GLOBALS['SITE_STRUCTURE']->findChild($xItem->object_id);
	$x = wps\node\RootNode::getInstance()->findChild($xItem->object_id);
	//if (isset($x)) echo $x->getPosition().' | ';
	if (isset($x)) {
		$position = $x->getPosition();
		
		if (!empty($position)) $xClasses[] = $position;
	}
	
	return $xClasses;
}

//add_filter('nav_menu_link_attributes', __NAMESPACE__.'\\nav_menu_link_attributes');
add_filter('nav_menu_css_class', __NAMESPACE__.'\\nav_menu_css_class', 10, 3);

/*
// Structure
class amekusa\WPSiteStructure\node\RootNode#2547 (5) {
  public $entity =>
  NULL
  public $children =>
  array(3) {
    'newsrelease' =>
    class amekusa\WPSiteStructure\node\PostNode#2548 (5) {
      public $entity =>
      NULL
      public $children =>
      array(1) {
        ...
      }
      public $parent =>
              ...

      public $label =>
      NULL
      public $isInvisible =>
      bool(false)
    }
    'topics' =>
    class amekusa\WPSiteStructure\node\PostNode#2550 (5) {
      public $entity =>
      class WP_Post#2539 (24) {
        ...
      }
      public $children =>
      array(1) {
        ...
      }
      public $parent =>
              ...

      public $label =>
      NULL
      public $isInvisible =>
      bool(false)
    }
    'itooshi' =>
    class amekusa\WPSiteStructure\node\PostNode#2541 (5) {
      public $entity =>
      class WP_Post#2536 (24) {
        ...
      }
      public $children =>
      array(1) {
        ...
      }
      public $parent =>
              ...

      public $label =>
      NULL
      public $isInvisible =>
      bool(false)
    }
  }
  public $parent =>
  NULL
  public $label =>
  string(4) "HOME"
  public $isInvisible =>
  bool(false)
}



// Menu
class WP_Post#2323 (40) {
  public $ID =>
  int(1949)
  public $post_author =>
  string(1) "1"
  public $post_date =>
  string(19) "2014-06-11 11:30:02"
  public $post_date_gmt =>
  string(19) "2014-06-11 02:30:02"
  public $post_content =>
  string(0) ""
  public $post_title =>
  string(4) "HOME"
  public $post_excerpt =>
  string(0) ""
  public $post_status =>
  string(7) "publish"
  public $comment_status =>
  string(4) "open"
  public $ping_status =>
  string(4) "open"
  public $post_password =>
  string(0) ""
  public $post_name =>
  string(4) "home"
  public $to_ping =>
  string(0) ""
  public $pinged =>
  string(0) ""
  public $post_modified =>
  string(19) "2014-06-11 12:21:51"
  public $post_modified_gmt =>
  string(19) "2014-06-11 03:21:51"
  public $post_content_filtered =>
  string(0) ""
  public $post_parent =>
  int(0)
  public $guid =>
  string(54) "http://bit-arrow.net.div2.prime-strategy.co.jp/?p=1949"
  public $menu_order =>
  int(1)
  public $post_type =>
  string(13) "nav_menu_item"
  public $post_mime_type =>
  string(0) ""
  public $comment_count =>
  string(1) "0"
  public $filter =>
  string(3) "raw"
  public $db_id =>
  int(1949)
  public $menu_item_parent =>
  string(1) "0"
  public $object_id =>
  string(4) "1949"
  public $object =>
  string(6) "custom"
  public $type =>
  string(6) "custom"
  public $type_label =>
  string(12) "カスタム"
  public $title =>
  string(4) "HOME"
  public $url =>
  string(47) "http://bit-arrow.net.div2.prime-strategy.co.jp/"
  public $target =>
  string(0) ""
  public $attr_title =>
  string(0) ""
  public $description =>
  string(0) ""
  public $classes =>
  array(7) {
    [0] =>
    string(0) ""
    [1] =>
    string(9) "menu-item"
    [2] =>
    string(21) "menu-item-type-custom"
    [3] =>
    string(23) "menu-item-object-custom"
    [4] =>
    string(17) "current-menu-item"
    [5] =>
    string(17) "current_page_item"
    [6] =>
    string(14) "menu-item-home"
  }
  public $xfn =>
  string(0) ""
  public $current =>
  bool(true)
  public $current_item_ancestor =>
  bool(false)
  public $current_item_parent =>
  bool(false)
}
class WP_Post#2553 (40) {
  public $ID =>
  int(1944)
  public $post_author =>
  string(1) "1"
  public $post_date =>
  string(19) "2014-06-11 11:13:27"
  public $post_date_gmt =>
  string(19) "2014-06-11 02:13:27"
  public $post_content =>
  string(1) " "
  public $post_title =>
  string(0) ""
  public $post_excerpt =>
  string(0) ""
  public $post_status =>
  string(7) "publish"
  public $comment_status =>
  string(4) "open"
  public $ping_status =>
  string(4) "open"
  public $post_password =>
  string(0) ""
  public $post_name =>
  string(4) "1944"
  public $to_ping =>
  string(0) ""
  public $pinged =>
  string(0) ""
  public $post_modified =>
  string(19) "2014-06-11 12:21:51"
  public $post_modified_gmt =>
  string(19) "2014-06-11 03:21:51"
  public $post_content_filtered =>
  string(0) ""
  public $post_parent =>
  int(0)
  public $guid =>
  string(54) "http://bit-arrow.net.div2.prime-strategy.co.jp/?p=1944"
  public $menu_order =>
  int(2)
  public $post_type =>
  string(13) "nav_menu_item"
  public $post_mime_type =>
  string(0) ""
  public $comment_count =>
  string(1) "0"
  public $filter =>
  string(3) "raw"
  public $db_id =>
  int(1944)
  public $menu_item_parent =>
  string(1) "0"
  public $object_id =>
  string(4) "1935"
  public $object =>
  string(4) "page"
  public $type =>
  string(9) "post_type"
  public $type_label =>
  string(15) "固定ページ"
  public $url =>
  string(59) "http://bit-arrow.net.div2.prime-strategy.co.jp/newsrelease/"
  public $title =>
  string(12) "NEWS RELEASE"
  public $target =>
  string(0) ""
  public $attr_title =>
  string(0) ""
  public $description =>
  string(0) ""
  public $classes =>
  array(4) {
    [0] =>
    string(0) ""
    [1] =>
    string(9) "menu-item"
    [2] =>
    string(24) "menu-item-type-post_type"
    [3] =>
    string(21) "menu-item-object-page"
  }
  public $xfn =>
  string(0) ""
  public $current =>
  bool(false)
  public $current_item_ancestor =>
  bool(false)
  public $current_item_parent =>
  bool(false)
}
class WP_Post#2554 (40) {
  public $ID =>
  int(1945)
  public $post_author =>
  string(1) "1"
  public $post_date =>
  string(19) "2014-06-11 11:13:27"
  public $post_date_gmt =>
  string(19) "2014-06-11 02:13:27"
  public $post_content =>
  string(1) " "
  public $post_title =>
  string(0) ""
  public $post_excerpt =>
  string(0) ""
  public $post_status =>
  string(7) "publish"
  public $comment_status =>
  string(4) "open"
  public $ping_status =>
  string(4) "open"
  public $post_password =>
  string(0) ""
  public $post_name =>
  string(4) "1945"
  public $to_ping =>
  string(0) ""
  public $pinged =>
  string(0) ""
  public $post_modified =>
  string(19) "2014-06-11 12:21:51"
  public $post_modified_gmt =>
  string(19) "2014-06-11 03:21:51"
  public $post_content_filtered =>
  string(0) ""
  public $post_parent =>
  int(0)
  public $guid =>
  string(54) "http://bit-arrow.net.div2.prime-strategy.co.jp/?p=1945"
  public $menu_order =>
  int(3)
  public $post_type =>
  string(13) "nav_menu_item"
  public $post_mime_type =>
  string(0) ""
  public $comment_count =>
  string(1) "0"
  public $filter =>
  string(3) "raw"
  public $db_id =>
  int(1945)
  public $menu_item_parent =>
  string(1) "0"
  public $object_id =>
  string(4) "1933"
  public $object =>
  string(4) "page"
  public $type =>
  string(9) "post_type"
  public $type_label =>
  string(15) "固定ページ"
  public $url =>
  string(54) "http://bit-arrow.net.div2.prime-strategy.co.jp/topics/"
  public $title =>
  string(6) "TOPICS"
  public $target =>
  string(0) ""
  public $attr_title =>
  string(0) ""
  public $description =>
  string(0) ""
  public $classes =>
  array(4) {
    [0] =>
    string(0) ""
    [1] =>
    string(9) "menu-item"
    [2] =>
    string(24) "menu-item-type-post_type"
    [3] =>
    string(21) "menu-item-object-page"
  }
  public $xfn =>
  string(0) ""
  public $current =>
  bool(false)
  public $current_item_ancestor =>
  bool(false)
  public $current_item_parent =>
  bool(false)
}
class WP_Post#2555 (40) {
  public $ID =>
  int(1946)
  public $post_author =>
  string(1) "1"
  public $post_date =>
  string(19) "2014-06-11 11:13:27"
  public $post_date_gmt =>
  string(19) "2014-06-11 02:13:27"
  public $post_content =>
  string(1) " "
  public $post_title =>
  string(0) ""
  public $post_excerpt =>
  string(0) ""
  public $post_status =>
  string(7) "publish"
  public $comment_status =>
  string(4) "open"
  public $ping_status =>
  string(4) "open"
  public $post_password =>
  string(0) ""
  public $post_name =>
  string(4) "1946"
  public $to_ping =>
  string(0) ""
  public $pinged =>
  string(0) ""
  public $post_modified =>
  string(19) "2014-06-11 12:21:51"
  public $post_modified_gmt =>
  string(19) "2014-06-11 03:21:51"
  public $post_content_filtered =>
  string(0) ""
  public $post_parent =>
  int(0)
  public $guid =>
  string(54) "http://bit-arrow.net.div2.prime-strategy.co.jp/?p=1946"
  public $menu_order =>
  int(4)
  public $post_type =>
  string(13) "nav_menu_item"
  public $post_mime_type =>
  string(0) ""
  public $comment_count =>
  string(1) "0"
  public $filter =>
  string(3) "raw"
  public $db_id =>
  int(1946)
  public $menu_item_parent =>
  string(1) "0"
  public $object_id =>
  string(4) "1931"
  public $object =>
  string(4) "page"
  public $type =>
  string(9) "post_type"
  public $type_label =>
  string(15) "固定ページ"
  public $url =>
  string(55) "http://bit-arrow.net.div2.prime-strategy.co.jp/itooshi/"
  public $title =>
  string(15) "イトオシ！"
  public $target =>
  string(0) ""
  public $attr_title =>
  string(0) ""
  public $description =>
  string(0) ""
  public $classes =>
  array(4) {
    [0] =>
    string(0) ""
    [1] =>
    string(9) "menu-item"
    [2] =>
    string(24) "menu-item-type-post_type"
    [3] =>
    string(21) "menu-item-object-page"
  }
  public $xfn =>
  string(0) ""
  public $current =>
  bool(false)
  public $current_item_ancestor =>
  bool(false)
  public $current_item_parent =>
  bool(false)
}
class WP_Post#2556 (40) {
  public $ID =>
  int(1947)
  public $post_author =>
  string(1) "1"
  public $post_date =>
  string(19) "2014-06-11 11:13:27"
  public $post_date_gmt =>
  string(19) "2014-06-11 02:13:27"
  public $post_content =>
  string(0) ""
  public $post_title =>
  string(39) "わが町わが職場<br />わが仲間"
  public $post_excerpt =>
  string(0) ""
  public $post_status =>
  string(7) "publish"
  public $comment_status =>
  string(4) "open"
  public $ping_status =>
  string(4) "open"
  public $post_password =>
  string(0) ""
  public $post_name =>
  string(4) "1947"
  public $to_ping =>
  string(0) ""
  public $pinged =>
  string(0) ""
  public $post_modified =>
  string(19) "2014-06-11 12:21:51"
  public $post_modified_gmt =>
  string(19) "2014-06-11 03:21:51"
  public $post_content_filtered =>
  string(0) ""
  public $post_parent =>
  int(0)
  public $guid =>
  string(54) "http://bit-arrow.net.div2.prime-strategy.co.jp/?p=1947"
  public $menu_order =>
  int(5)
  public $post_type =>
  string(13) "nav_menu_item"
  public $post_mime_type =>
  string(0) ""
  public $comment_count =>
  string(1) "0"
  public $filter =>
  string(3) "raw"
  public $db_id =>
  int(1947)
  public $menu_item_parent =>
  string(1) "0"
  public $object_id =>
  string(4) "1684"
  public $object =>
  string(4) "page"
  public $type =>
  string(9) "post_type"
  public $type_label =>
  string(15) "固定ページ"
  public $url =>
  string(54) "http://bit-arrow.net.div2.prime-strategy.co.jp/mytown/"
  public $title =>
  string(39) "わが町わが職場<br />わが仲間"
  public $target =>
  string(0) ""
  public $attr_title =>
  string(0) ""
  public $description =>
  string(0) ""
  public $classes =>
  array(4) {
    [0] =>
    string(0) ""
    [1] =>
    string(9) "menu-item"
    [2] =>
    string(24) "menu-item-type-post_type"
    [3] =>
    string(21) "menu-item-object-page"
  }
  public $xfn =>
  string(0) ""
  public $current =>
  bool(false)
  public $current_item_ancestor =>
  bool(false)
  public $current_item_parent =>
  bool(false)
}
class WP_Post#2557 (40) {
  public $ID =>
  int(1948)
  public $post_author =>
  string(1) "1"
  public $post_date =>
  string(19) "2014-06-11 11:13:27"
  public $post_date_gmt =>
  string(19) "2014-06-11 02:13:27"
  public $post_content =>
  string(1) " "
  public $post_title =>
  string(0) ""
  public $post_excerpt =>
  string(0) ""
  public $post_status =>
  string(7) "publish"
  public $comment_status =>
  string(4) "open"
  public $ping_status =>
  string(4) "open"
  public $post_password =>
  string(0) ""
  public $post_name =>
  string(4) "1948"
  public $to_ping =>
  string(0) ""
  public $pinged =>
  string(0) ""
  public $post_modified =>
  string(19) "2014-06-11 12:21:51"
  public $post_modified_gmt =>
  string(19) "2014-06-11 03:21:51"
  public $post_content_filtered =>
  string(0) ""
  public $post_parent =>
  int(0)
  public $guid =>
  string(54) "http://bit-arrow.net.div2.prime-strategy.co.jp/?p=1948"
  public $menu_order =>
  int(6)
  public $post_type =>
  string(13) "nav_menu_item"
  public $post_mime_type =>
  string(0) ""
  public $comment_count =>
  string(1) "0"
  public $filter =>
  string(3) "raw"
  public $db_id =>
  int(1948)
  public $menu_item_parent =>
  string(1) "0"
  public $object_id =>
  string(4) "1649"
  public $object =>
  string(4) "page"
  public $type =>
  string(9) "post_type"
  public $type_label =>
  string(15) "固定ページ"
  public $url =>
  string(53) "http://bit-arrow.net.div2.prime-strategy.co.jp/world/"
  public $title =>
  string(18) "当世海外事情"
  public $target =>
  string(0) ""
  public $attr_title =>
  string(0) ""
  public $description =>
  string(0) ""
  public $classes =>
  array(4) {
    [0] =>
    string(0) ""
    [1] =>
    string(9) "menu-item"
    [2] =>
    string(24) "menu-item-type-post_type"
    [3] =>
    string(21) "menu-item-object-page"
  }
  public $xfn =>
  string(0) ""
  public $current =>
  bool(false)
  public $current_item_ancestor =>
  bool(false)
  public $current_item_parent =>
  bool(false)
}
class WP_Post#2558 (40) {
  public $ID =>
  int(1943)
  public $post_author =>
  string(1) "1"
  public $post_date =>
  string(19) "2014-06-11 11:13:27"
  public $post_date_gmt =>
  string(19) "2014-06-11 02:13:27"
  public $post_content =>
  string(1) " "
  public $post_title =>
  string(0) ""
  public $post_excerpt =>
  string(0) ""
  public $post_status =>
  string(7) "publish"
  public $comment_status =>
  string(4) "open"
  public $ping_status =>
  string(4) "open"
  public $post_password =>
  string(0) ""
  public $post_name =>
  string(4) "1943"
  public $to_ping =>
  string(0) ""
  public $pinged =>
  string(0) ""
  public $post_modified =>
  string(19) "2014-06-11 12:21:51"
  public $post_modified_gmt =>
  string(19) "2014-06-11 03:21:51"
  public $post_content_filtered =>
  string(0) ""
  public $post_parent =>
  int(0)
  public $guid =>
  string(54) "http://bit-arrow.net.div2.prime-strategy.co.jp/?p=1943"
  public $menu_order =>
  int(7)
  public $post_type =>
  string(13) "nav_menu_item"
  public $post_mime_type =>
  string(0) ""
  public $comment_count =>
  string(1) "0"
  public $filter =>
  string(3) "raw"
  public $db_id =>
  int(1943)
  public $menu_item_parent =>
  string(1) "0"
  public $object_id =>
  string(4) "1938"
  public $object =>
  string(4) "page"
  public $type =>
  string(9) "post_type"
  public $type_label =>
  string(15) "固定ページ"
  public $url =>
  string(60) "http://bit-arrow.net.div2.prime-strategy.co.jp/hotteam/vol7/"
  public $title =>
  string(9) "HOT TEAM!"
  public $target =>
  string(0) ""
  public $attr_title =>
  string(0) ""
  public $description =>
  string(0) ""
  public $classes =>
  array(4) {
    [0] =>
    string(0) ""
    [1] =>
    string(9) "menu-item"
    [2] =>
    string(24) "menu-item-type-post_type"
    [3] =>
    string(21) "menu-item-object-page"
  }
  public $xfn =>
  string(0) ""
  public $current =>
  bool(false)
  public $current_item_ancestor =>
  bool(false)
  public $current_item_parent =>
  bool(false)
}
class WP_Post#2559 (40) {
  public $ID =>
  int(1953)
  public $post_author =>
  string(1) "1"
  public $post_date =>
  string(19) "2014-06-11 12:21:51"
  public $post_date_gmt =>
  string(19) "2014-06-11 03:21:51"
  public $post_content =>
  string(1) " "
  public $post_title =>
  string(0) ""
  public $post_excerpt =>
  string(0) ""
  public $post_status =>
  string(7) "publish"
  public $comment_status =>
  string(4) "open"
  public $ping_status =>
  string(4) "open"
  public $post_password =>
  string(0) ""
  public $post_name =>
  string(4) "1953"
  public $to_ping =>
  string(0) ""
  public $pinged =>
  string(0) ""
  public $post_modified =>
  string(19) "2014-06-11 12:21:51"
  public $post_modified_gmt =>
  string(19) "2014-06-11 03:21:51"
  public $post_content_filtered =>
  string(0) ""
  public $post_parent =>
  int(0)
  public $guid =>
  string(54) "http://bit-arrow.net.div2.prime-strategy.co.jp/?p=1953"
  public $menu_order =>
  int(8)
  public $post_type =>
  string(13) "nav_menu_item"
  public $post_mime_type =>
  string(0) ""
  public $comment_count =>
  string(1) "0"
  public $filter =>
  string(3) "raw"
  public $db_id =>
  int(1953)
  public $menu_item_parent =>
  string(1) "0"
  public $object_id =>
  string(4) "1950"
  public $object =>
  string(4) "page"
  public $type =>
  string(9) "post_type"
  public $type_label =>
  string(15) "固定ページ"
  public $url =>
  string(58) "http://bit-arrow.net.div2.prime-strategy.co.jp/backnumber/"
  public $title =>
  string(27) "Arrow バックナンバー"
  public $target =>
  string(0) ""
  public $attr_title =>
  string(0) ""
  public $description =>
  string(0) ""
  public $classes =>
  array(4) {
    [0] =>
    string(0) ""
    [1] =>
    string(9) "menu-item"
    [2] =>
    string(24) "menu-item-type-post_type"
    [3] =>
    string(21) "menu-item-object-page"
  }
  public $xfn =>
  string(0) ""
  public $current =>
  bool(false)
  public $current_item_ancestor =>
  bool(false)
  public $current_item_parent =>
  bool(false)
}
*/
?>