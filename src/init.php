<?php
namespace amekusa\WPSiteStructure;

/**
 * WP Site Structure
 *
 * @author amekusa <post@amekusa.com>
 */

// Namespace include guard [
if (defined(__NAMESPACE__ . '\LOADED')) return;
const LOADED = true;
// ]

require_once __DIR__ . '/functions.php';
require_once __DIR__ . '/node/functions.php';

/*
require_once __DIR__ . '/Walker.php';
require_once __DIR__ . '/Widget.php';

require_once __DIR__ . '/node/package.php';
require_once __DIR__ . '/drafts/package.php';
*/
?>