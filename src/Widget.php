<?php
namespace amekusa\WPSiteStructure;

use amekusa\WPELib as wpe;

class Widget extends \WP_Widget {
	
	public static function register() {
		register_widget(__CLASS__);
	}
	
	public static function unregister() {
		unregister_widget(__CLASS__);
	}
	
	public function __construct() {
		parent::__construct('com_amekusa_wps_submenu', __('Structural Sub Menu'), array (
				'description' => __('Structural Sub Menu')));
	}
	
	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget($args, $instance) {
		$title = apply_filters('widget_title', $instance['title']);
		echo $args['before_widget'];
		if (!empty($title)) echo $args['before_title'] . $title . $args['after_title'];
		// @formatter:off [
?>
<div class="body">
	<p>Widget Content</p>
</div>
<?php
		// ] @formatter:on
		echo $args['after_widget'];
	}
	
	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form($instance) {
		if (isset($instance['title'])) {
			$title = $instance['title'];
		} else {
			$title = __('New title');
		}
		
		// @formatter:off [
?>
<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
</p>
<?php
		// ] @formatter:on
	}
	
	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update($new_instance, $old_instance) {
		$instance = array ();
		$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		return $instance;
	}
}
?>