<?php
namespace amekusa\WPSiteStructure;

use amekusa\WPELib as wpe;

class Walker extends \Walker {
	
	public function __construct() {
	}
	
	public function start_lvl(&$xOutput, $xDepth = 0, $xArgs = array()) {
		$indent = str_repeat("\t", $xDepth);
		$xOutput .= "\n$indent<ul class=\"sub-menu\">\n";
	}
	
	public function end_lvl(&$xOutput, $xDepth = 0, $xArgs = array()) {
		$indent = str_repeat("\t", $xDepth);
		$xOutput .= "$indent</ul>\n";
	}
	
	public function start_el(&$xOutput, $xItem, $xDepth = 0, $xArgs = array(), $xId = 0) {
		$indent = ($xDepth) ? str_repeat("\t", $xDepth) : '';
	
	}
	
	public function end_el(&$xOutput, $xItem, $xDepth = 0, $xArgs = array()) {
		$xOutput .= "</li>\n";
	}
}
?>